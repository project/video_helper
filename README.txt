Video helper
------------

You will be able to create a simple link in your template to show a video tutorial. Individually for all Drupal pages.

Installation:
-------------
drush en video_helper -y
OR
Go to /admin/modules and enable this module

Configuration:
--------------
On page /admin/config/content/video_helper you can provide a link (youtube, external link, anything basically) what containts your video.
Wildcards are supported like node/%. This is meaning that you link will appear on all node pages.

Usage example:
--------------
Put the folowing code into you page.tpl.php. Onto any place wher you would like to see the video tutorial.

      <?php if(function_exists('video_helper_get_help')) : ?>
        <?php if(video_helper_get_help()) : ?>
          <li id="video_helper">
            <a href="<?php echo video_helper_get_help(); ?>" target="_blank"><?php echo t('Video Tutorial'); ?><span id="helper-icon-bg"><i class="fa fa-video-camera"></i></span></a>
          </li>
        <?php endif; ?>
      <?php endif; ?>

Have fun! ;)
